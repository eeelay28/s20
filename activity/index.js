
let number = Number(prompt("Give me a number:"));

console.log("The number you provided is: " + number);


for( let n = number; n >= 0; n--) {

  if(n<=50){
    break;
  }  
  
  if (n % 10 === 0){
    console.log("This number is divisible by ten:" + n + " proceeding to the next iteration");
    continue;
  }  
  
  if (n % 5 === 0){
    console.log("Divisible by five:" + n);
    continue;
  }
}


// part two

let word = "supercalifragilisticexpialidocious";
let consonants = "";

console.log("Word of the Day: " + word);
for (let n = 0; n < word.length; n++) {
  if (
    word[n].toLowerCase() == "a" ||
    word[n].toLowerCase() == "e" ||
    word[n].toLowerCase() == "i" ||
    word[n].toLowerCase() == "o" ||
    word[n].toLowerCase() == "u"
  ) {
    continue;
  } else {
    consonants += word[n];
  }
}

console.log("These are the consonants: " + consonants);
